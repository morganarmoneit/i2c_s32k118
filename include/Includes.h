/*
 * Includes.h
 *
 *  Created on: Jun 20, 2019
 *      Author: joshd
 */

#ifndef INCLUDES_H_
#define INCLUDES_H_

#include "Cpu.h"
#include "clockMan1.h"
//#include "flexio_i2c1.h"
#include "lpi2c.h"
#include "pin_mux.h"
#include "rtcTimer1.h"
#include "adConv1.h"
#include <stdio.h>
#include <stdlib.h>

//Bit accessible operations, easily portable between processors.
#define BIT(var, bitmask)			((byte)(((var) & (bitmask)) != 0))
#define BCLR(var, bitmask)			(((var) &= ~(bitmask)))
#define BSET(var, bitmask)			(((var) |= (bitmask)))
#define BTOGGLE(var, bitmask)		(((var) ^= (bitmask)))

//Type definitions
typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;

#include "RTI.h"
#include "i2c.h"
#include "Initialize.h"
#include "adc.h"
#include "prox.h"
#include "edma.h"
#include "state_timer.h"
#include "LEDs.h"
#include "spi.h"
#include "WS2812.h"
#include "mux.h"
#include "heat_transfer.h"

/* Enum for bag slot location, double sided */
//enum eWarmer {
//	eWARMER1, eWARMER2, eWARMER3, eWARMER4, eWARMER5,
//
//	eWARMER6, eWARMER7, eWARMER8, eWARMER9, eWARMER10, eWARMER_COUNT,
//};

/* Enum for bag slot location, single sided */
enum eWarmer {
	eWARMER1, eWARMER2, eWARMER3, eWARMER4, eWARMER5, eWARMER_COUNT,
};

struct sWarmer {
	bool BAG_IN;
	uint8_t STATE;
	uint32_t TIME_SECONDS;
	float BAG_TEMP;
	uint16_t PROX_BUFFER[3];
	uint16_t PROX_EMPTY;
	uint8_t BAG_OUT_COUNTDOWN;   //5 second countdown
	uint8_t READY_BRIGHTNESS;  //255 to 0 over 5 second countdown
};
extern struct sWarmer Warmer[eWARMER_COUNT];

enum eState {
	eEMPTY = 0,
	eWARMING = 1,
	eREADY = 2,
	eREADY_COUNTDOWN = 3,
	eEXPIRED = 4,
	eINOP = 5,
	eSTATE_COUNT,
};

struct sTemperature {
	WORD ONE_MIN_OF_TEMP[7];
	uint8_t temp_index;
	float set_temp;
	float cavity_temp;
};
extern struct sTemperature Temperature;

bool demo_mode;
uint16_t demo_timeout;
uint8_t red_brightness;
uint8_t green_brightness;
uint8_t blue_brightness;
bool yellow_fault_on;

#endif /* INCLUDES_H_ */
