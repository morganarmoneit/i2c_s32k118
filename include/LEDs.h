/*
 * LEDs.h
 *
 *  Created on: Aug 8, 2019
 *      Author: morgana
 */

#ifndef LEDS_H_
#define LEDS_H_

struct sLed_Setup {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
};

extern struct sLed_Setup sLed[];

void All_off_LEDs(void);
void All_blue_LEDs(void);
void Single_blue_LED(int x);
void All_white_LEDs(void);
void All_rainbow_LEDs(void);
void Inc_green_LEDs (int x);
void Single_green_LED(int x);
void Single_off_LED(int x);
void Single_red_LED(int x);
void Process_LEDs(void);
//void led_update(void);

#endif /* LEDS_H_ */
