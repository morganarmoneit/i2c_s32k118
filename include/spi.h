/*
 * spi.h
 *
 *  Created on: Aug 27 2019
 *      Author: JEFPL8
 */

#ifndef spi_H_
#define spi_H_

extern flexio_spi_master_state_t spiMasterState;
extern flexio_device_state_t flexIODeviceState;

void InitFlexSPI(void);

#endif /* spi_H_ */
