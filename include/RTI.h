/*
 * RTI.h
 *
 *  Created on: Jun 20, 2019
 *      Author: joshd
 */

#ifndef RTI_H_
#define RTI_H_

#define RTI_INTERRUPT_RATE()    (128)

#define RTI_60s()         		(RTI_1000ms() * 60)
#define RTI_10s()         		(RTI_1000ms() * 10)
#define RTI_5s()         		(RTI_1000ms() * 5)
#define RTI_1000ms()            (RTI_INTERRUPT_RATE())
#define RTI_500ms()             (RTI_1000ms() / 2)
#define RTI_250ms()             (RTI_1000ms() / 4)
#define RTI_125ms()             (RTI_1000ms() / 8)
#define RTI_62_50ms()           (RTI_1000ms() / 16)
#define RTI_31_25ms()           (RTI_1000ms() / 32)
#define RTI_15_625ms()          (RTI_1000ms() / 64)
#define RTI_7_8125ms()          (RTI_1000ms() / 128)

enum TPMxSC
{
	TOF		= 0x80,	//Timer Overflow Flag.
	TOIE	= 0x40	//Timer Overflow Interrupt Enable	[0 = disabled, 1 = enabled]
};

union uRTIFlags
{
	BYTE B;
	struct
	{
		BYTE Task7_8125ms   	: 1;
		BYTE Task15_625ms		: 1;
		BYTE Task31_25ms		: 1;
		BYTE Task62_5ms			: 1;
		BYTE Task125ms			: 1;
		BYTE Task250ms			: 1;
		BYTE Task500ms			: 1;
		BYTE Task1000ms     	: 1;
		BYTE Task5s				: 1;
		BYTE Task10s			: 1;
		BYTE Task60s			: 1;
	} b;
};
extern volatile union uRTIFlags RTIFlags;

void InitRTI(void);

#endif /* RTI_H_ */
