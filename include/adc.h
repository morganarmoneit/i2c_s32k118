/*
 * adc.h
 *
 *  Created on: Jun 20, 2019
 *      Author: joshd
 */

#ifndef ADC_H_
#define ADC_H_

#define ADC_VREFH       4.6f
#define ADC_VREFL       0.0f

extern WORD adcMax;

void InitADC(void);
void ReadADC(void);

#endif /* ADC_H_ */
