/*
 * i2c.h
 *
 *  Created on: Jun 20, 2019
 *      Author: joshd
 */

#ifndef I2C_H_
#define I2C_H_

//Comment out flexio I2C driver to allow flexio to be used for SPI
//extern flexio_i2c_master_state_t i2cMasterState;
//extern flexio_device_state_t flexIODeviceState;

extern lpi2c_master_state_t lpi2c1MasterState;

//void InitFlexI2C(void);
void InitLPI2C(void);

#endif /* I2C_H_ */
