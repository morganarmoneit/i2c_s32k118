/*
 * Initialize.h
 *
 *  Created on: Jun 20, 2019
 *      Author: joshd
 */

#ifndef INITIALIZE_H_
#define INITIALIZE_H_

void InitHardware(void);
void InitSoftware(void);
void InitMCU(void);
void InitGPIO(void);


#endif /* INITIALIZE_H_ */
