/*
 *  prox.h
 *
 *  Created on: Jun 27, 2019
 *      Author: JEFPL8
 */

#ifndef prox_H_
#define prox_H_

struct sMux_Setup {
	int port;
	int address;
} Mux;
extern const struct sMux_Setup caMux[];

void measure_single_prox(uint8_t lcv);
void measure_all_prox(void);
void read_all_prox(void);
void Init_vncl(void);
void Cal_vncl(void);
void set_current_hi(uint8_t lcv);
void set_current_lo(uint8_t lcv);

#endif
