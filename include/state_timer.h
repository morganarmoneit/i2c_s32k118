/*
 * ready_led.h
 *
 *  Created on: July 32, 2019
 *      Author: JEFPL8
 */

void Demo_detect(void);
void Bag_transition(void);
void Bag_in_detection (void);
void Bag_out_detection(void);
void timer_incr(void);
void status_update(void);
void Set_bag_type_and_bag_out_threshold(void);
