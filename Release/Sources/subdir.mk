################################################################################
# Automatically-generated file. Do not edit!
################################################################################

-include ../makefile.local

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS_QUOTED += \
"../Sources/Initialize.c" \
"../Sources/LEDs.c" \
"../Sources/RTI.c" \
"../Sources/WS2812.c" \
"../Sources/adc.c" \
"../Sources/edma.c" \
"../Sources/heat_transfer.c" \
"../Sources/i2c.c" \
"../Sources/main.c" \
"../Sources/mux.c" \
"../Sources/prox.c" \
"../Sources/spi.c" \
"../Sources/state_timer.c" \

C_SRCS += \
../Sources/Initialize.c \
../Sources/LEDs.c \
../Sources/RTI.c \
../Sources/WS2812.c \
../Sources/adc.c \
../Sources/edma.c \
../Sources/heat_transfer.c \
../Sources/i2c.c \
../Sources/main.c \
../Sources/mux.c \
../Sources/prox.c \
../Sources/spi.c \
../Sources/state_timer.c \

OBJS_OS_FORMAT += \
./Sources/Initialize.o \
./Sources/LEDs.o \
./Sources/RTI.o \
./Sources/WS2812.o \
./Sources/adc.o \
./Sources/edma.o \
./Sources/heat_transfer.o \
./Sources/i2c.o \
./Sources/main.o \
./Sources/mux.o \
./Sources/prox.o \
./Sources/spi.o \
./Sources/state_timer.o \

C_DEPS_QUOTED += \
"./Sources/Initialize.d" \
"./Sources/LEDs.d" \
"./Sources/RTI.d" \
"./Sources/WS2812.d" \
"./Sources/adc.d" \
"./Sources/edma.d" \
"./Sources/heat_transfer.d" \
"./Sources/i2c.d" \
"./Sources/main.d" \
"./Sources/mux.d" \
"./Sources/prox.d" \
"./Sources/spi.d" \
"./Sources/state_timer.d" \

OBJS += \
./Sources/Initialize.o \
./Sources/LEDs.o \
./Sources/RTI.o \
./Sources/WS2812.o \
./Sources/adc.o \
./Sources/edma.o \
./Sources/heat_transfer.o \
./Sources/i2c.o \
./Sources/main.o \
./Sources/mux.o \
./Sources/prox.o \
./Sources/spi.o \
./Sources/state_timer.o \

OBJS_QUOTED += \
"./Sources/Initialize.o" \
"./Sources/LEDs.o" \
"./Sources/RTI.o" \
"./Sources/WS2812.o" \
"./Sources/adc.o" \
"./Sources/edma.o" \
"./Sources/heat_transfer.o" \
"./Sources/i2c.o" \
"./Sources/main.o" \
"./Sources/mux.o" \
"./Sources/prox.o" \
"./Sources/spi.o" \
"./Sources/state_timer.o" \

C_DEPS += \
./Sources/Initialize.d \
./Sources/LEDs.d \
./Sources/RTI.d \
./Sources/WS2812.d \
./Sources/adc.d \
./Sources/edma.d \
./Sources/heat_transfer.d \
./Sources/i2c.d \
./Sources/main.d \
./Sources/mux.d \
./Sources/prox.d \
./Sources/spi.d \
./Sources/state_timer.d \


# Each subdirectory must supply rules for building sources it contributes
Sources/Initialize.o: ../Sources/Initialize.c
	@echo 'Building file: $<'
	@echo 'Executing target #22 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/Initialize.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/Initialize.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/LEDs.o: ../Sources/LEDs.c
	@echo 'Building file: $<'
	@echo 'Executing target #23 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/LEDs.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/LEDs.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/RTI.o: ../Sources/RTI.c
	@echo 'Building file: $<'
	@echo 'Executing target #24 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/RTI.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/RTI.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/WS2812.o: ../Sources/WS2812.c
	@echo 'Building file: $<'
	@echo 'Executing target #25 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/WS2812.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/WS2812.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/adc.o: ../Sources/adc.c
	@echo 'Building file: $<'
	@echo 'Executing target #26 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/adc.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/adc.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/edma.o: ../Sources/edma.c
	@echo 'Building file: $<'
	@echo 'Executing target #27 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/edma.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/edma.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/heat_transfer.o: ../Sources/heat_transfer.c
	@echo 'Building file: $<'
	@echo 'Executing target #28 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/heat_transfer.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/heat_transfer.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/i2c.o: ../Sources/i2c.c
	@echo 'Building file: $<'
	@echo 'Executing target #29 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/i2c.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/i2c.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/main.o: ../Sources/main.c
	@echo 'Building file: $<'
	@echo 'Executing target #30 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/main.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/main.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/mux.o: ../Sources/mux.c
	@echo 'Building file: $<'
	@echo 'Executing target #31 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/mux.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/mux.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/prox.o: ../Sources/prox.c
	@echo 'Building file: $<'
	@echo 'Executing target #32 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/prox.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/prox.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/spi.o: ../Sources/spi.c
	@echo 'Building file: $<'
	@echo 'Executing target #33 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/spi.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/spi.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Sources/state_timer.o: ../Sources/state_timer.c
	@echo 'Building file: $<'
	@echo 'Executing target #34 $<'
	@echo 'Invoking: Standard S32DS C Compiler'
	arm-none-eabi-gcc "@Sources/state_timer.args" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "Sources/state_timer.o" "$<"
	@echo 'Finished building: $<'
	@echo ' '


