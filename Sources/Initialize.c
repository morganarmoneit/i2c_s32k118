/*
 * Initialize.c
 *
 *  Created on: Jun 20, 2019
 *      Author: joshd
 */

#include "Includes.h"

void InitHardware(void)
{
	InitMCU();
	InitGPIO();
}

void InitSoftware(void)
{
	InitEDMA();
	InitLPI2C();
	InitADC();
	InitRTI();
	InitFlexSPI();
	WS2812_Init();
	Init_mux();
	Init_vncl();
	Cal_vncl();
	ReadADC();
	Demo_detect();
	All_off_LEDs();
}

void InitMCU(void)
{
	/* Initialize and configure clocks
	 *  -   see clock manager component for details
	 */
	CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
						g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
	CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);
}

void InitGPIO(void)
{
	/* Initialize pins
	 *  -   See PinSettings component for more info
	 */
	PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

}
