/*
 * state_timer.c
 *
 *  Created on: July 31, 2019
 *      Author: JEFPL8
 */

#include "Includes.h"

// Demo header pin
#define	DEMO_PIN		8

/*minimum time (60 minutes) a bag can be in warmer before shifting to READY state
because the PCB lacks an RTC, a 128hz interrupt is used to generate timing, this interrupt runs
slow on all boards assembled for pre-production.  The int takes 1.1 sec to per 128 cycles, so this
variable has to be offset to compensate */
//nominal value for when an RTC is added
//uint16_t MINIMUM_WARMING = 3600;
//offset value for using the 128 hz interrupt
uint16_t MINIMUM_WARMING = 3276;

/*transition time from warming to ready in seconds (60 sec) */
//nominal value for when an RTC is added
//uint16_t WARMING_TO_READY = 60;
//offset value for using the 128 hz interrupt
uint16_t WARMING_TO_READY = 54;

/*transition time from ready to expired in seconds (14 days) */
//nominal value for when an RTC is added
//uint32_t READY_TO_EXPIRED = 1209600;
//offset value for using the 128 hz interrupt
uint32_t READY_TO_EXPIRED = 1100736;

// if prox at ref designator U1 reads over 10k counts, then go to demo mode and overwrite transition values
void Demo_detect(void) {

	if (Warmer[0].PROX_EMPTY > 10000) {

		//set to demo mode
		demo_mode = true;

		//use slot 1's empty cal value for slot 0
		Warmer[0].PROX_EMPTY = Warmer[1].PROX_EMPTY;

		//overwrite transition time from ready to expired for 5 min (value in seconds)
		//nominal value for when an RTC is added
		//READY_TO_EXPIRED = 300;
		//offset value for using the 128 hz interrupt
		READY_TO_EXPIRED = 273;

		All_white_LEDs();

		//  1s delay
		for (uint32_t delay = 0; delay < 1900000; delay++) {
		}
	}
}

void Bag_transition(void) {

	/*  For each warming slot... */
	for (uint8_t lcv = 0; lcv < eWARMER_COUNT; lcv++) {

		int16_t range = 0;

		range = Warmer[lcv].PROX_BUFFER[2] - Warmer[lcv].PROX_BUFFER[1];

/*******BAG_IN*********************************************************/
		if ((Warmer[lcv].BAG_IN == false)
				&& (Warmer[lcv].STATE != eINOP)) {

//			once the signal range has stabilized
			if ((range < 20)
				&& (range > -20)) {

//				check if latest three measurements are 100 counts above empty value
				if (((Warmer[lcv].PROX_EMPTY + 100) < Warmer[lcv].PROX_BUFFER[0])
					&& ((Warmer[lcv].PROX_EMPTY + 100) < Warmer[lcv].PROX_BUFFER[1])
					&& ((Warmer[lcv].PROX_EMPTY + 100) < Warmer[lcv].PROX_BUFFER[2])) {

//					set BAG_IN
					Warmer[lcv].BAG_IN = true;

				}
			}
		}

/*******BAG_OUT*************************************************************/
		else if ((Warmer[lcv].TIME_SECONDS > 2) || (Warmer[lcv].BAG_IN == true)) {

//			once the signal range has stabilized
			if ((range < 20)
					&& (range > -20)) {


//				check if latest three measurements are below empty value + 100 counts
				if (((Warmer[lcv].PROX_EMPTY + 100) > Warmer[lcv].PROX_BUFFER[0])
						&& ((Warmer[lcv].PROX_EMPTY + 100) > Warmer[lcv].PROX_BUFFER[1])
						&& ((Warmer[lcv].PROX_EMPTY + 100) > Warmer[lcv].PROX_BUFFER[2])){

						/*  go to bag out state */
						Warmer[lcv].BAG_IN = false;

				}
			}
		}
	}
}

/*This must be called 1/second*/
void timer_incr(void) {
	PTE->PSOR = 1UL << 9;//indicate timer increment
	for (int lcv = 0; lcv < eWARMER_COUNT; lcv++) {

		//if there is a bag in and not expired (prevent rollover and restart)...
		if (Warmer[lcv].BAG_IN == true && Warmer[lcv].STATE != eEXPIRED) {

			//... then increment counter
			Warmer[lcv].TIME_SECONDS++;

//			if (Warmer[lcv].TIME_SECONDS == 600){
//				PTE->PCOR = 1UL << 9;//indicate timer increment
//			}
		}
	}
	PTE->PCOR = 1UL << 9;//indicate timer increment
}

//loop takes 0.066ms
//This must be called faster than 1 per second
void status_update(void) {
//	PTE->PSOR = 1UL << 9;//indicate timer increment
	for (int lcv = 0; lcv < eWARMER_COUNT; lcv++) {

		//transition to WARMING while in normal use
		if ((Warmer[lcv].BAG_IN == true && (Warmer[lcv].STATE != eREADY))
				&& ((Warmer[lcv].BAG_TEMP < (Temperature.set_temp - 1.1)) || (Warmer[lcv].TIME_SECONDS < MINIMUM_WARMING))
				&& (!demo_mode)
				&& (Warmer[lcv].STATE != eINOP)) {

			Warmer[lcv].STATE = eWARMING;
		}

		//transition to WARMING while in demo
		else if ((Warmer[lcv].BAG_IN == true && (Warmer[lcv].STATE != eREADY))
				&& (Warmer[lcv].TIME_SECONDS < WARMING_TO_READY)
				&& (demo_mode)
				&& (Warmer[lcv].STATE != eINOP)) {

			Warmer[lcv].STATE = eWARMING;
		}

		// transition to READY - demo or normal use
		else if ((Warmer[lcv].BAG_IN == true)
				&& (Warmer[lcv].TIME_SECONDS < READY_TO_EXPIRED)
				&& (Warmer[lcv].STATE != eINOP)) {

			Warmer[lcv].STATE = eREADY;
			Warmer[lcv].BAG_OUT_COUNTDOWN = 10;
			Warmer[lcv].READY_BRIGHTNESS = 255;
		}

		// If at READY status, the bag is removed and 5 second countdown is started
		else if ((Warmer[lcv].BAG_IN == false)
				&& (Warmer[lcv].STATE == eREADY)
				&& (Warmer[lcv].STATE != eINOP)) {

			// Start countdown
			if (Warmer[lcv].BAG_OUT_COUNTDOWN != 0) {

				Warmer[lcv].BAG_OUT_COUNTDOWN =
						((Warmer[lcv].BAG_OUT_COUNTDOWN) - 1);
			}

			// After 5 seconds, go to EMPTY status
			else {
				Warmer[lcv].STATE = eEMPTY;
			}
		}

		//transition to EXPIRED
		else if ((Warmer[lcv].BAG_IN == true)
				&& (Warmer[lcv].TIME_SECONDS >= READY_TO_EXPIRED)
				&& (Warmer[lcv].STATE != eINOP)) {

			Warmer[lcv].STATE = eEXPIRED;
		}

		// if there is no prox calibration value, then assume sensor is not working and go to INOP
		else if (Warmer[lcv].PROX_BUFFER[2] <= 10 || Warmer[lcv].STATE == eINOP) {

			Warmer[lcv].STATE = eINOP;
		}

		//transition to EMPTY
		else {

			Warmer[lcv].STATE = eEMPTY;
			Warmer[lcv].TIME_SECONDS = 0;
			Warmer[lcv].BAG_OUT_COUNTDOWN = 10;
			Warmer[lcv].READY_BRIGHTNESS = 255;
			Warmer[lcv].BAG_TEMP = 25.0;
		}
	}
//	PTE->PCOR = 1UL << 9;//indicate timer increment
}


