/*
 * i2c.c
 *
 *  Created on: Jun 20, 2019
 *      Author: joshd
 */

/***** NO CHANGES MADE TO THIS CODE *****/
#include "Includes.h"

/* Define data transfer size */
#define TRANSFER_SIZE (1u)

//Comment out flexio I2C driver to allow flexio to be used for SPI
//flexio_i2c_master_state_t i2cMasterState;
//flexio_device_state_t flexIODeviceState;

lpi2c_master_state_t lpi2cMasterState;

//Comment out flexio I2C driver to allow flexio to be used for SPI
//void InitFlexI2C(void)
//{
//	/* Allocate the memory necessary for the FlexIO state structures */
//
//	/* Init FlexIO device */
//	FLEXIO_DRV_InitDevice(INST_FLEXIO_I2C1, &flexIODeviceState);
//
//	/* Initialize FlexIO I2C driver as bus master */
//	FLEXIO_I2C_DRV_MasterInit(INST_FLEXIO_I2C1, &flexio_i2c1_MasterConfig0, &i2cMasterState);
//}

void InitLPI2C(void) {
	LPI2C_DRV_MasterDeinit(INST_LPI2C);

	LPI2C_DRV_MasterInit(INST_LPI2C, &lpi2c_MasterConfig_left,
			&lpi2cMasterState);
}
