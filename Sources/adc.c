/*
 * adc.c
 *
 *  Created on: Jun 20, 2019
 *      Author: joshd
 */

#include "Includes.h"
#include "adc_driver.h"
#include "adc_hw_access.h"

WORD adcMax;

//float THERMISTOR_ARRAY[600];
//uint16_t therm = 0;

/* Lookup table for Vishay thermistor B= 3940, Vrefh = 4.6VDC */
static const WORD cawPCBThermistorLookup[] = {
//(degF)     +0    +1    +2    +3    +4    +5    +6    +7    +8    +9
		/*   0*/403, 416, 428, 441, 455, 468, 482, 496, 510, 525,
		/*  10*/540, 555, 570, 586, 602, 619, 635, 652, 670, 687,
		/*  20*/705, 723, 742, 760, 779, 799, 818, 838, 858, 878,
		/*  30*/899, 920, 941, 962, 984, 1006, 1028, 1050, 1073, 1096,
		/*  40*/1119, 1142, 1165, 1189, 1213, 1237, 1261, 1285, 1309, 1334,
		/*  50*/1359, 1384, 1409, 1434, 1459, 1484, 1509, 1535, 1560, 1586,
		/*  60*/1612, 1637, 1663, 1689, 1715, 1740, 1766, 1792, 1818, 1843,
		/*  70*/1869, 1895, 1920, 1946, 1971, 1997, 2022, 2048, 2073, 2098,
		/*  80*/2123, 2148, 2172, 2197, 2221, 2246, 2270, 2294, 2318, 2341,
		/*  90*/2365, 2388, 2411, 2434, 2457, 2480, 2502, 2525, 2547, 2569,
		/* 100*/2590, 2612, 2633, 2654, 2675, 2695, 2716, 2736, 2756, 2776,
		/* 110*/2795, 2814, 2833, 2852, 2871, 2889, 2907, 2925, 2943, 2960,
		/* 120*/2978, 2995, 3012, 3028, 3045, 3061, 3077, 3092, 3108, 3123,
		/* 130*/3138, 3153, 3168, 3182, 3196, 3210, 3224, 3238, 3251, 3264,
		/* 140*/3277, 3290, 3303, 3315, 3327, 3339, 3351, 3363, 3374, 3386,
		/* 150*/3397, 3408, 3419, 3429, 3440, 3450, 3460, 3470, 3480, 3490,
		/* 160*/3499, 3508, 3518, 3527, 3535, 3544, 3553, 3561, 3570, 3578,
		/* 170*/3586, 3594, 3601, 3609, 3617, 3624, 3631, 3638, 3645, 3652,
		/* 180*/3659, 3666, 3672, 3679, 3685, 3692, 3698, 3704, 3710, 3716,
		/* 190*/3721, 3727, 3733, 3738, 3743, 3749, 3754, 3759, 3764, 3769,
		/* 200*/3774, 3779, 3783, 3788, 3793, 3797, 3801, 3806, 3810, 3814,
		/* 210*/3818, 3822, 3826, 3830, 3834, 3838, 3842, 3845, 3849, 3852,
		/* 220*/3856, 3859, 3863, 3866, 3869, 3872, 3876, 3879, 3882, 3885,
		/* 230*/3888, 3891, 3894, 3896, 3899, 3902, 3905, 3907, 3907, 3912,
		/* 240*/3915, 3917, 3920, 3922, 3924, 3927, 3929, 3931, 3934, 3936,
		/* 250*/3938, 3940, 3942, 3944, 3946, 3948, 3950, 3952, 3954, 3956,
		/* 260*/3958, 3959, 3961, 3963, 3965, 3966, 3968, 3970, 3971, 3973,
		/* 270*/3974, 3976, 3977, 3979, 3980, 3982, 3983, 3985, 3986, 3988,
		/* 280*/3989, 3990, 3992, 3993, 3994, 3995, 3997, 3998, 3999, 4000,
		/* 290*/4001, 4002, 4004, 4005, 4006, 4007, 4008, 4009, 4010, 4011,
		/* 300*/4012, 4013, 4014 };

/* Lookup table for Murata thermistor B=3900 */
//static const WORD cawPCBThermistorLookup[] = {
////(degF)     +0    +1    +2    +3    +4    +5    +6    +7    +8    +9
///*   0*/  524,  537,  550,  564,  578,  592,  606,  621,  635,  650,
///*  10*/  665,  681,  696,  712,  728,  745,  761,  778,  795,  812,
///*  20*/  830,  847,  865,  883,  901,  920,  938,  957,  976,  996,
///*  30*/ 1015, 1035, 1054, 1074, 1094, 1115, 1135, 1156, 1177, 1198,
///*  40*/ 1219, 1240, 1261, 1283, 1304, 1326, 1348, 1370, 1392, 1414,
///*  50*/ 1436, 1458, 1481, 1503, 1526, 1548, 1571, 1593, 1616, 1639,
///*  60*/ 1662, 1684, 1707, 1730, 1753, 1776, 1799, 1821, 1844, 1867,
///*  70*/ 1890, 1912, 1935, 1958, 1980, 2003, 2025, 2048, 2070, 2092,
///*  80*/ 2114, 2136, 2158, 2180, 2202, 2224, 2245, 2267, 2288, 2309,
///*  90*/ 2330, 2351, 2372, 2393, 2413, 2434, 2454, 2474, 2494, 2514,
///* 100*/ 2534, 2553, 2572, 2592, 2611, 2630, 2648, 2667, 2685, 2703,
///* 110*/ 2721, 2739, 2757, 2775, 2792, 2809, 2826, 2843, 2860, 2876,
///* 120*/ 2892, 2908, 2924, 2940, 2956, 2971, 2986, 3001, 3016, 3031,
///* 130*/ 3046, 3060, 3074, 3088, 3102, 3116, 3129, 3143, 3156, 3169,
///* 140*/ 3182, 3194, 3207, 3219, 3231, 3243, 3255, 3267, 3278, 3290,
///* 150*/ 3301, 3312, 3323, 3334, 3345, 3355, 3365, 3376, 3386, 3396,
///* 160*/ 3406, 3415, 3425, 3434, 3443, 3452, 3461, 3470, 3479, 3488,
///* 170*/ 3496, 3505, 3513, 3521, 3529, 3537, 3545, 3552, 3560, 3567,
///* 180*/ 3575, 3582, 3589, 3596, 3603, 3610, 3617, 3623, 3630, 3636,
///* 190*/ 3642, 3649, 3655, 3661, 3667, 3673, 3678, 3684, 3690, 3695,
///* 200*/ 3701, 3706, 3711, 3717, 3722, 3727, 3732, 3737, 3742, 3746,
///* 210*/ 3751, 3756, 3760, 3765, 3769, 3774, 3778, 3782, 3786, 3790,
///* 220*/ 3794, 3798, 3802, 3806, 3810, 3814, 3817, 3821, 3825, 3828,
///* 230*/ 3832, 3835, 3838, 3842, 3845, 3848, 3851, 3855, 3858, 3861,
///* 240*/ 3864, 3867, 3870, 3873, 3875, 3878, 3881, 3884, 3886, 3889,
///* 250*/ 3892, 3894, 3897, 3899, 3902, 3904, 3906, 3909, 3911, 3913,
///* 260*/ 3916, 3918, 3920, 3922, 3924, 3926, 3928, 3930, 3932, 3934,
///* 270*/ 3936, 3938, 3940, 3942, 3944, 3946, 3948, 3949, 3951, 3953,
///* 280*/ 3954, 3956, 3958, 3959, 3961, 3963, 3964, 3966, 3967, 3969,
///* 290*/ 3970, 3972, 3973, 3974, 3976, 3977, 3979, 3980, 3981, 3983,
///* 300*/ 3984, 3985, 3986
//};
void InitADC(void) {

	/* Configure and calibrate the ADC converter
	 *  -   See ADC component for the configuration details
	 */
	ADC_DRV_ConfigConverter(INST_ADCONV1, &adConv1_ConvConfig0);

	/*  clear user configured gain */
	ADC0->UG = 0x004;
	ADC0->USR_OFS = 0x00;

	ADC_DRV_AutoCalibration(INST_ADCONV1);

	/* Get ADC max value from the resolution */
	if (adConv1_ConvConfig0.resolution == ADC_RESOLUTION_8BIT) {
		adcMax = (uint16_t) (1 << 8);
	} else if (adConv1_ConvConfig0.resolution == ADC_RESOLUTION_10BIT) {
		adcMax = (uint16_t) (1 << 10);
	} else {
		adcMax = (uint16_t) (1 << 12);
	}

	/*Take first reading*/
	ReadADC();
}

/* called every 10 seconds, this function fills 1 minutes worth of data into an array.  The median
 * value becomes the derived set temperature and the most recent measurement becomes the air
 * temperature for calculation of heat transfer over last 10 seconds */
void ReadADC(void) {
//	PTE->PSOR = 1UL << 9;//indicate timer increment

	WORD adcRawValue = 0;
	WORD LO_TO_HI_TEMPS[7];

	//increment index
	if (Temperature.temp_index < 6) {
		Temperature.temp_index++;
	}
	else{
		Temperature.temp_index = 0;
	}

	/* Configure ADC channel and software trigger a conversion */
	ADC_DRV_ConfigChan(INST_ADCONV1, 0U, &adConv1_ChnConfig0);

	/* Wait for the conversion to be done */
	ADC_DRV_WaitConvDone(INST_ADCONV1);

	/* Store the channel result into a local variable */
	ADC_DRV_GetChanResult(INST_ADCONV1, 0U, &adcRawValue);

	WORD wLow = 0;
	WORD wHigh = sizeof(cawPCBThermistorLookup) - 1;
	WORD wMid;

	while (wLow < wHigh) {
		wMid = (wLow + wHigh) / 2;  //Locate the midpoint.

		if (adcRawValue > cawPCBThermistorLookup[wMid]) {
			wLow = wMid + 1;
		} else {
			wHigh = wMid;
		}
	}

	/* Convert lookup table value to floating point temperature in 0.1C resolution */
	Temperature.cavity_temp = (((float)wLow) - 32) * 5 / 9;

	/* add integer temperature (in deg F) into one minute array */
	Temperature.ONE_MIN_OF_TEMP[Temperature.temp_index] = wLow;

	//copy struct array into local array to be sorted
	for (int i = 0; i < 7; i++) {
		LO_TO_HI_TEMPS[i] = Temperature.ONE_MIN_OF_TEMP[i];
	}

	// sort lcoal array from low to high
	int compare(const void * a, const void * b) {
		return (*(uint16_t*) a - *(uint16_t*) b);
	}
	qsort(LO_TO_HI_TEMPS, 7, sizeof(uint16_t), compare);

	/* use median value as the assumed set temperature of the warmer */
	Temperature.set_temp = (((float)LO_TO_HI_TEMPS[3]) -32) * 5 / 9;

//	PTE->PCOR = 1UL << 9;//indicate timer increment
}

