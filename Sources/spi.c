/*
 * spi.c
 *
 *  Created on: Aug 27 2019
 *      Author: JEFPL8
 */


#include "Includes.h"

/* Define data transfer size */
#define TRANSFER_SIZE (1u)

flexio_spi_master_state_t spiMasterState;
flexio_device_state_t flexIODeviceState;

void InitFlexSPI(void)
{
	/* Allocate the memory necessary for the FlexIO state structures */

	/* Init FlexIO device */
	FLEXIO_DRV_InitDevice(INST_FLEXIO_SPI1, &flexIODeviceState);

	/* Initialize FlexIO SPI driver as bus master */
	FLEXIO_SPI_DRV_MasterInit(INST_FLEXIO_SPI1, &WS2818_config, &spiMasterState);
}
