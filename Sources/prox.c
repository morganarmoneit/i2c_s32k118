/*
 * prox.c
 *
 *  Created on: June 25, 2019
 *      Author: JEFPL8
 */

#include "Includes.h"

///* Define data transfer size */
#define TRANSFER_SIZE   1u
#define TRANSFER_SIZE_2 2u
#define TRANSFER_SIZE_3 3u
#define TRANSFER_SIZE_4 4u

uint8_t PROX_PORT_NUMBER[] = { 0 };
uint8_t PROX_PORT_BUFFER[] = { 0 };
uint8_t PROX_RESULT_REGISTER[] = { 0x87 }; /*resultant register for prox reads */

uint8_t PROX_CMD_REG_ADDRESS[] = { 0x80 };
uint8_t PROX_CMD_SETUP[] = { 0x80, 0x00 };
uint8_t PROX_CMD_ON_DEMAND[] = { 0x80, 0x08 };
uint8_t PROX_CMD_BUFFER[] = { 0x00 };

uint8_t PROX_CURRENT_ADDRESS[] = { 0x83 };
uint8_t PROX_CURRENT_200mA[] = { 0x83, 0x14 };
uint8_t PROX_CURRENT_BUFFER[] = { 0x00 };

uint8_t PROX_MOD_TIME_ADDRESS[] = { 0x8F };
uint8_t PROX_MOD_TIME[] = { 0x8F, 0x00 };
uint8_t PROX_MOD_TIME_BUFFER[] = { 0x00 };

/*  debugging - delete before release */
uint16_t PROX_READS [10] [255];
uint8_t prox_index = 0;

///* struct for double sided board */
//const struct sMux_Setup caMux[] = { { 0x01, 0x77 }, { 0x02, 0x77 },
//		{ 0x04, 0x77 }, { 0x08, 0x77 }, { 0x10, 0x77 }, { 0x01, 0x70 }, { 0x02,
//				0x70 }, { 0x04, 0x70 }, { 0x08, 0x70 }, { 0x10, 0x70 } };

/* struct for single sided board */
const struct sMux_Setup caMux[] = { { 0x01, 0x77 }, { 0x02, 0x77 },
		{ 0x04, 0x77 }, { 0x08, 0x77 }, { 0x10, 0x77 } };

 /* 5ms  */
void measure_all_prox(void) {

	/* Loop that increments the MUX ports by one */
	for (uint8_t lcv = 0; lcv < eWARMER_COUNT; lcv++) {

		/* Set mux I2C address (0x70 or 0x77) and mux port (0x01, 0x02, 0x04, 0x08, or 0x10) */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, caMux[lcv].address, false);
		PROX_PORT_NUMBER[0] = caMux[lcv].port;
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_PORT_NUMBER,
		TRANSFER_SIZE,
		true, 10u);

		/* Set I2C address to 0x13, base for all prox sensors */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, 0x13, false);

		/* Write to prox command register to start measurement */
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_CMD_ON_DEMAND,
		TRANSFER_SIZE_2, true, 10u);

		/* Super important to avoid mux cross talk - disable all mux ports when finished */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, caMux[lcv].address,
		false);
		PROX_PORT_NUMBER[0] = 0;
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_PORT_NUMBER,
		TRANSFER_SIZE,
		true, 10u);
	}
}

/* 7ms */
void read_all_prox(void) {
	uint8_t PROX_READ_BUFFER[] = { 0x00, 0x00 };

	/* Loop that increments the MUX ports by one */
	for (uint8_t lcv = 0; lcv < eWARMER_COUNT; lcv++) {

		//shift array elements
		Warmer[lcv].PROX_BUFFER[0] = Warmer[lcv].PROX_BUFFER[1];
		Warmer[lcv].PROX_BUFFER[1] = Warmer[lcv].PROX_BUFFER[2];

		/* Set mux I2C address (0x70 or 0x77) and mux port (0x01, 0x02, 0x04, 0x08, or 0x10) */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, caMux[lcv].address, false);
		PROX_PORT_NUMBER[0] = caMux[lcv].port;
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_PORT_NUMBER,
		TRANSFER_SIZE,
		true, 10u);

		/* Set I2C address to 0x13, base for all prox sensors */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, 0x13, false);

		//Sent I2C command to access result register
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_RESULT_REGISTER,
		TRANSFER_SIZE, true, 10u);

		//Request two 8 bit integers for result data
		LPI2C_DRV_MasterReceiveDataBlocking(INST_LPI2C, PROX_READ_BUFFER,
		TRANSFER_SIZE_2, true, 10u);

		//Add 16-bit integer into moving average array @ element 2
		Warmer[lcv].PROX_BUFFER[2] = PROX_READ_BUFFER[0] * 256 + PROX_READ_BUFFER[1];

		PROX_READS[lcv][prox_index] = Warmer[lcv].PROX_BUFFER[2];

		if (prox_index == 254 && lcv == 9) {
			prox_index = 0;
		}
		else if (lcv == 9) {
			prox_index++;
		}

		/* Super important to avoid mux cross talk - disable all mux ports when finished */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, caMux[lcv].address,
		false);
		PROX_PORT_NUMBER[0] = 0;
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_PORT_NUMBER,
		TRANSFER_SIZE,
		true, 10u);

	}
}

void Init_vncl(void) {

	uint8_t PROX_READ_BUFFER[] = { 0x00, 0x00 };

	/* Loop that increments the MUX ports by one */
	for (uint8_t lcv = 0; lcv < eWARMER_COUNT; lcv++) {

		/* Set mux I2C address (0x70 or 0x77) and send mux which port should be used to transmit (0x01, 0x02, 0x04, 0x08, or 0x10) */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, caMux[lcv].address,
		false);
		PROX_PORT_NUMBER[0] = caMux[lcv].port;
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_PORT_NUMBER,
		TRANSFER_SIZE,
		true, 10u);

		/* Set I2C address to 0x13, base for all prox sensors */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, 0x13, false);

		/* Write to prox command register to disable measurement and allow adjustment */
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_CMD_SETUP,
		TRANSFER_SIZE_2, true, 10u);

		/* Write to prox current register to set 200mA IR driver current */
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_CURRENT_200mA,
		TRANSFER_SIZE_2, true, 10u);

		/* Read from prox current register and set state to INOP if not set correctly */
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_CURRENT_ADDRESS,
		TRANSFER_SIZE, true, 10u);
		LPI2C_DRV_MasterReceiveDataBlocking(INST_LPI2C, PROX_CURRENT_BUFFER,
		TRANSFER_SIZE, true, 10u);

		if (PROX_CURRENT_BUFFER[0] != PROX_CURRENT_200mA[1]) {
			Warmer[lcv].STATE = eINOP;
		}

		/* Write to prox mod timing register */
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_MOD_TIME,
		TRANSFER_SIZE_2, true, 10u);

		/* Read from prox mod timing register and set state to INOP if not set correctly */
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_MOD_TIME_ADDRESS,
		TRANSFER_SIZE, true, 10u);
		LPI2C_DRV_MasterReceiveDataBlocking(INST_LPI2C, PROX_MOD_TIME_BUFFER,
		TRANSFER_SIZE, true, 10u);

		if (PROX_MOD_TIME_BUFFER[0] != PROX_MOD_TIME[1]) {
			Warmer[lcv].STATE = eINOP;
		}

		/* Write to prox command register to start measurement */
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_CMD_ON_DEMAND,
		TRANSFER_SIZE_2, true, 10u);

		//Sent I2C command to access result register
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_RESULT_REGISTER,
		TRANSFER_SIZE, true, 10u);

		//Request two 8 bit integers for result data
		LPI2C_DRV_MasterReceiveDataBlocking(INST_LPI2C, PROX_READ_BUFFER,
		TRANSFER_SIZE_2, true, 10u);

//		/* Write to prox command register to re enable self timed prox measurement */
//		PROX_CMD_SELF_TIMED[1] = 0x03;
//		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_CMD_SELF_TIMED,
//		TRANSFER_SIZE_2, true, 10u);

//		/* Read from prox command register to enable set prox_en and selftimed_en */
//		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_CMD_REG_ADDRESS,
//		TRANSFER_SIZE, true, 10u);
//		LPI2C_DRV_MasterReceiveDataBlocking(INST_LPI2C, PROX_CMD_BUFFER,
//		TRANSFER_SIZE, true, 10u);

//		/* Read from prox rate register */
//		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_RATE_ADDRESS,
//		TRANSFER_SIZE, true, 10u);
//		LPI2C_DRV_MasterReceiveDataBlocking(INST_LPI2C, PROX_RATE_BUFFER,
//		TRANSFER_SIZE, true, 10u);

//		/* Read from prox current register */
//		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_CURRENT_ADDRESS,
//		TRANSFER_SIZE, true, 10u);
//		LPI2C_DRV_MasterReceiveDataBlocking(INST_LPI2C, PROX_CURRENT_BUFFER,
//		TRANSFER_SIZE, true, 10u);

		/* Super important to avoid mux cross talk - disable all mux ports when finished */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, caMux[lcv].address,
		false);
		PROX_PORT_NUMBER[0] = 0;
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_PORT_NUMBER,
		TRANSFER_SIZE,
		true, 10u);
	}
}

void Cal_vncl(void) {

	uint8_t PROX_READ_BUFFER[] = { 0x00, 0x00 };

	All_blue_LEDs();

	/* Loop that increments the MUX ports by one */
	for (uint8_t lcv = 0; lcv < eWARMER_COUNT; lcv++) {

		Single_off_LED(lcv);

		/* Set mux I2C address (0x70 or 0x77) and send mux which port should be used to transmit (0x01, 0x02, 0x04, 0x08, or 0x10) */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, caMux[lcv].address,
		false);
		PROX_PORT_NUMBER[0] = caMux[lcv].port;
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_PORT_NUMBER,
		TRANSFER_SIZE,
		true, 10u);

		/* Set I2C address to 0x13, base for all prox sensors */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, 0x13, false);

		// measure each prox with slot empty and use as baseline to determine when bag is present
		for (int index = 0; index < 3; index++) {

			uint16_t count_result = 0x0000;

			/* Write to prox command register to start measurement */
			LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_CMD_ON_DEMAND,
			TRANSFER_SIZE_2, true, 10u);

			/* Enter while loop until command register signals measurement is complete */
			while (PROX_CMD_BUFFER[0] != 0xA0) {
				/* Read from prox command register to enable set prox_en and selftimed_en */
				LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C,
						PROX_CMD_REG_ADDRESS,
						TRANSFER_SIZE, true, 10u);
				LPI2C_DRV_MasterReceiveDataBlocking(INST_LPI2C, PROX_CMD_BUFFER,
				TRANSFER_SIZE, true, 10u);
			}

			//Sent I2C command to access result register
			LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_RESULT_REGISTER,
			TRANSFER_SIZE, true, 10u);

			//Request two 8 bit integers for result data
			LPI2C_DRV_MasterReceiveDataBlocking(INST_LPI2C, PROX_READ_BUFFER,
			TRANSFER_SIZE_2, true, 10u);

			/* Adding two 8-bit integers into one 16-bit integer */
			count_result = PROX_READ_BUFFER[0] * 256 + PROX_READ_BUFFER[1];

			Warmer[lcv].PROX_BUFFER[index] = count_result;

			//  4ms delay
			for (int delay = 0; delay < 32000; delay++) {
			}
		}

		/* sort PROX_BUFFER from low to high*/
		int compare(const void * a, const void * b) {
			return (*(uint16_t*) a - *(uint16_t*) b);
		}
		qsort(Warmer[lcv].PROX_BUFFER, 3, sizeof(uint16_t), compare);

//		if buffer has null value or a value similar to the measurement of a iv bag....
		if ((Warmer[lcv].PROX_BUFFER[1] == 0)
				|| ((Warmer[lcv].PROX_BUFFER[1] > 3500)
						&& (Warmer[lcv].PROX_BUFFER[1] < 9999))){

//			Set INOP state for all warmer slots, unit has to be reset so the calibration can be accurate
			for (uint8_t i = 0; i < eWARMER_COUNT; i++) {
				Warmer[i].STATE = eINOP;
			}
		}

//		otherwise load buffer location 3 as the empty cavity value and turn on green led to indicate circuit is good to go*/
		else {
			Warmer[lcv].PROX_EMPTY = Warmer[lcv].PROX_BUFFER[1];

			Single_green_LED(lcv);

			//  1s delay to green LED to be illuminated
			for (uint32_t delay = 0; delay < 200000; delay++) {
			}
		}

		//  1s delay
		for (uint32_t delay = 0; delay < 150000; delay++) {
		}

		/* Super important to avoid mux cross talk - disable all mux ports when finished */
		LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, caMux[lcv].address,
		false);
		PROX_PORT_NUMBER[0] = 0;
		LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, PROX_PORT_NUMBER,
		TRANSFER_SIZE,
		true, 10u);
	}
}
