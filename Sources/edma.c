/*
 * edma.c
 *
 *  Created on: Jun 27, 2019
 *      Author: JEFPL8
 */

/********** NO CHANGES MADE TO THIS CODE **********/
#include "Includes.h"

void InitEDMA(void) {
//	Please initialize DMA module by calling before LPI2C initialization.
	EDMA_DRV_Init(&dmaController1_State, &dmaController1_InitConfig0,
			edmaChnStateArray, edmaChnConfigArray,
			EDMA_CONFIGURED_CHANNELS_COUNT);
}
