/* ###################################################################
 **     Filename    : main.c
 **     Processor   : S32K1xx
 **     Abstract    :
 **         Main module.
 **         This module contains user's application code.
 **     Settings    :
 **     Contents    :
 **         No public methods
 **
 ** ###################################################################*/
/*!
 ** @file main.c
 ** @version 01.00
 ** @brief
 **         Main module.
 **         This module contains user's application code.
 */
/*!
 **  @addtogroup main_module main module documentation
 **  @{
 */
/* MODULE main */

/* Including necessary module. Cpu.h contains other modules needed for compiling.*/
#include "Cpu.h"
#include "Includes.h"

volatile int exit_code = 0;

/* User includes (#include below this line is not maintained by Processor Expert) */
/* Switch to supervisor mode */
void swSupervisorMode(void) {
	__asm(" MOVS R0, #0x0 ");
	__asm(" MSR CONTROL, R0 ");
	__asm("DSB");
	__asm("ISB");
}

/* Switch to user mode */
void swUserMode(void) {
	__asm(" MOVS R0, #0x1 ");
	__asm(" MSR CONTROL, R0 ");
	__asm("DSB");
	__asm("ISB");
}

/* SVC Exception interrupt */
void SVC_Handler(void) {
	/* Switch to supervisor mode need to be done through an exception handler*/
	swSupervisorMode();
}

/*! 
 \brief The main function for the project.
 \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
 */


struct sWarmer Warmer[eWARMER_COUNT];
struct sTemperature Temperature;

int main(void) {

	/* Write your local variable definition here */
	bool measure = true;

//	/* debugging and test arrays */
//	float BAG_TEMP_ARRAY [5] [250];
//	uint16_t minute = 0;

	/* Setting initial conditions for each slot (only right side slots) */
	for (int lcv = 0; lcv < eWARMER_COUNT; lcv++) {
		Warmer[lcv].STATE = eEMPTY;
		Warmer[lcv].TIME_SECONDS = 0;
		Warmer[lcv].BAG_OUT_COUNTDOWN = 10;
		Warmer[lcv].READY_BRIGHTNESS = 255;
		Warmer[lcv].BAG_TEMP = 25.0;

	}

	red_brightness = 0xFF;
	green_brightness = 0x77;
	blue_brightness = 0xFF;

	/* load temperature array with 75F data */
	for (int lcv = 0; lcv < 7; lcv++) {
		Temperature.ONE_MIN_OF_TEMP[lcv] = 75;
	}
	Temperature.set_temp = 41.0;  //assume 104F set temperature
	Temperature.cavity_temp = 25.0;

	/*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
#ifdef PEX_RTOS_INIT
	PEX_RTOS_INIT(); /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
#endif
	/*** End of Processor Expert internal initialization.                    ***/

	/* Write your code here */
	InitHardware();
	InitSoftware();

	for (;;) {

		//main super loop
		if (RTIFlags.b.Task7_8125ms) {
			RTIFlags.b.Task7_8125ms = false;

		}

		if (RTIFlags.b.Task15_625ms) {
			RTIFlags.b.Task15_625ms = false;

			if (measure == true) {
//				trigger a measurement of each prox sensor
				measure_all_prox();

//				toggle measure bool
				measure = !measure;
			}
			else {
//				read each prox sensor
				read_all_prox();

//				toggle measure bool
				measure = !measure;

//				make decision based on new values
				Bag_transition();
			}
		}

		if (RTIFlags.b.Task31_25ms) {
			RTIFlags.b.Task31_25ms = false;
		}

		if (RTIFlags.b.Task62_5ms) {
			RTIFlags.b.Task62_5ms = false;

//			Push new array of data to LEDs
			Process_LEDs();

//			HALT test code
//			All_white_LEDs();

		}

		if (RTIFlags.b.Task125ms) {
			RTIFlags.b.Task125ms = false;
		}

		if (RTIFlags.b.Task250ms) {
			RTIFlags.b.Task250ms = false;
		}

		if (RTIFlags.b.Task500ms) {
			RTIFlags.b.Task500ms = false;

//			check each bag warming timer and update status
			status_update();
		}

		if (RTIFlags.b.Task1000ms) {
			RTIFlags.b.Task1000ms = false;

//			must be called 1/second or timer will be inaccurate
			timer_incr();

//			if in demo mode...
			if ((demo_mode == true)
					&& (demo_timeout < 14400)) {

//				increment timer
				demo_timeout++;
			}

//			if demo timeout is over 4 hours...
			else if ((demo_mode == true)
					&& (demo_timeout >= 14400)) {

//				turn each warmer slot to state INOP
				for (int lcv = 0; lcv < eWARMER_COUNT; lcv++) {
					Warmer[lcv].STATE = eINOP;
				}
			}

//			turn each warmer slot to state INOP
//			for (int lcv = 0; lcv < eWARMER_COUNT; lcv++) {
				if ((Warmer[0].STATE == eINOP) && (red_brightness == 0xFF)) {
					red_brightness = 0x00;
					green_brightness = 0x00;
					blue_brightness = 0x00;
				}
				else if ((Warmer[0].STATE == eINOP) && (red_brightness == 0x00)) {
					red_brightness = 0xFF;
					green_brightness = 0x40;
				}
//			}
		}

		if (RTIFlags.b.Task5s) {
			RTIFlags.b.Task5s = false;
		}

		if (RTIFlags.b.Task10s) {
			RTIFlags.b.Task10s = false;

//			check internal cavity temperature
			ReadADC();

//			calculate amount of heat transfer
			Calc_heat();

		}

		if (RTIFlags.b.Task60s) {
			RTIFlags.b.Task60s = false;

//			//	/*  debugging array below - remove for production code */
//			/*  For each warming slot... */
//			for (uint8_t lcv = 0; lcv < 5; lcv++) {
//				BAG_TEMP_ARRAY[lcv][minute] = Warmer[lcv].BAG_TEMP;
//
//				if (lcv == 4) {
//					if (minute < 250) {
//						minute++;
//					} else {
//
//					}
//				}
//			}
//			/*  debugging array above - remove for production code */
		}
	}

	/* For example: for(;;) { } */

	/*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
 ** @}
 */
/*
 ** ###################################################################
 **
 **     This file was created by Processor Expert 10.1 [05.21]
 **     for the NXP S32K series of microcontrollers.
 **
 ** ###################################################################
 */
