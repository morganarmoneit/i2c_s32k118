/*
 * RTI.c
 *
 *  Created on: Jun 20, 2019
 *      Author: joshd
 */

#include "Includes.h"

volatile static int RTICounter;
volatile union uRTIFlags RTIFlags;

 /*! rtcTimer1 Seconds Callback */
 void rtcTimer1_SecIntCallback0(void)
 {
	RTICounter++;

	if(RTICounter % RTI_7_8125ms())
	{
		return;
	}

	RTIFlags.b.Task7_8125ms = true;

	if((RTICounter % RTI_15_625ms()) == 0)
	{
		RTIFlags.b.Task15_625ms = true;
	}

	if((RTICounter % RTI_31_25ms()) == 0)
	{
		RTIFlags.b.Task31_25ms = true;
	}

	if((RTICounter % RTI_62_50ms()) == 0)
	{
		RTIFlags.b.Task62_5ms = true;
	}

	if((RTICounter % RTI_125ms()) == 0)
	{
		RTIFlags.b.Task125ms = true;
	}

	if((RTICounter % RTI_250ms()) == 0)
	{
		RTIFlags.b.Task250ms = true;
	}

	if((RTICounter % RTI_500ms()) == 0)
	{
		RTIFlags.b.Task500ms = true;
	}

	if(RTICounter % RTI_1000ms() == 0)
	{
		RTIFlags.b.Task1000ms = true;
	}

	if(RTICounter % RTI_5s() == 0)
	{
		RTIFlags.b.Task5s = true;
	}

	if(RTICounter % RTI_10s() == 0)
	{
		RTIFlags.b.Task10s = true;
	}

	if(RTICounter == RTI_60s())
	{
		RTICounter = 0;

		RTIFlags.b.Task60s = true;
	}
 }

 /*! rtcTimer1 Fault Callback */
 void rtcTimer1_FaultIntCallback0(void)
 {

 }

 void InitRTI(void)
 {
	/* Initialize RTC instance
	 *  - See RTC configuration component for options
	 */
	RTC_DRV_Init(RTCTIMER1, &rtcTimer1_Config0);
	/* Configure RTC Time Seconds Interrupt */
	RTC_DRV_ConfigureSecondsInt(RTCTIMER1, &rtcTimer1_SecIntConfig0);

	/* Set the time and date */
	RTC_DRV_SetTimeDate(RTCTIMER1, &rtcTimer1_StartTime0);

	/* Start the RTC counter */
	RTC_DRV_StartCounter(RTCTIMER1);
 }
