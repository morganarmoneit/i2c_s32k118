/*
 * LEDs.c
 *
 *  Created on: Jul 31, 2019
 *      Author: morgana
 */

#include "Includes.h"

/************************** UPDATED CODE 8/9/19 *************************/
///* struct for double sided board */
//struct sLed_Setup sLed[eWARMER_COUNT] = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 },
//		{ 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0,
//				0 }, { 0, 0, 0 } };

/* struct for single sided board */
struct sLed_Setup sLed[eWARMER_COUNT] = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 },
		{ 0, 0, 0 }, { 0, 0, 0 } };

/* This define calculates the needed number of 32bit words needed. */
#define	ALIGN_32BIT_WORDS(x)	    ((3*(x)+3)/4)

static uint32_t led_data[ALIGN_32BIT_WORDS(eWARMER_COUNT + 1)]; /* plus one dummy LED at the end to drive signal low */

static void SetLEDColor(uint8_t led, uint8_t red, uint8_t green, uint8_t blue) {
	ws2812_rgb *p = (ws2812_rgb*) led_data;
	if (led <= eWARMER_COUNT) { /* within index */
		p[led].red = red;
		p[led].green = green;
		p[led].blue = blue;
	}
}

void All_off_LEDs(void) {
	for (int i = 0; i < eWARMER_COUNT; i++) {

//		for any slot not in INOP state...
		if ( Warmer[i].STATE != eINOP ) {

//			turn the LED off
			SetLEDColor(i, 0x00, 0x00, 0x00); /* all off */
		}
	}
	WS2812_OutputDataDMA(led_data, sizeof(led_data) / sizeof(int32_t));
}

void All_blue_LEDs(void) {
	for (int i = 0; i < eWARMER_COUNT; i++) {
		SetLEDColor(i, 0x00, 0x00, 0xFF); /* all blue */
	}
	WS2812_OutputDataDMA(led_data, sizeof(led_data) / sizeof(int32_t));
}

void Single_blue_LED(int x) {
	SetLEDColor(x, 0x00, 0x00, 0xFF); /* LED number "x" to blue */
	WS2812_OutputDataDMA(led_data, sizeof(led_data) / sizeof(int32_t));
}

void All_white_LEDs(void) {
	for (int i = 0; i < eWARMER_COUNT; i++) {
		SetLEDColor(i, 0xFF, 0xFF, 0xFF); /* all white */
	}
	WS2812_OutputDataDMA(led_data, sizeof(led_data) / sizeof(int32_t));
}

void All_rainbow_LEDs(void) {

	int r =0;
	int g = 0;
	int b = 0;

	for (r = 100; r < 255; r++) {
		for (g = 100; g < 255; g++) {
			for (b = 100; b < 255; b++) {

				for (int i = 0; i < eWARMER_COUNT; i++) {
					SetLEDColor(i, r, g, b); /* all white */
				}
				WS2812_OutputDataDMA(led_data, sizeof(led_data) / sizeof(int32_t));

			}
		}
	}
}

void Single_green_LED(int x) {
	SetLEDColor(x, 0x00, 0xFF, 0x00); /* LED number "x" to green */
	WS2812_OutputDataDMA(led_data, sizeof(led_data) / sizeof(int32_t));
}

void Inc_green_LEDs(int x) {
	for (int i = 0; i < x; i++) {
		SetLEDColor(i, 0x00, 0xFF, 0x00); /* all green */
	}
	WS2812_OutputDataDMA(led_data, sizeof(led_data) / sizeof(int32_t));
}

void Single_off_LED(int x) {
	SetLEDColor(x, 0x00, 0x00, 0x00); /* LED number "x" to off */
	WS2812_OutputDataDMA(led_data, sizeof(led_data) / sizeof(int32_t));
}

void Single_red_LED(int x) {
	SetLEDColor(x, 0xFF, 0x00, 0x00); /* LED number "x" to red */
	WS2812_OutputDataDMA(led_data, sizeof(led_data) / sizeof(int32_t));
}

void Process_LEDs(void) {
	static uint8_t blue = 0; /*start blue leds off for breathing */
	static uint8_t inhale = 1;
	struct sWarmer *ptrWarmer;

	/* Breathe dimming routine to indicate warming status */
	if (inhale) {
		blue += 5;
		if (blue == 250) {
			inhale = 0;
		}
	} else {
		blue -= 5;
		if (blue == 20) {
			inhale = 1;
		}
	}

	/* Routine for switching LED color based on struct warmer state */
	for (int lcv = 0; lcv < eWARMER_COUNT; lcv++) {
		ptrWarmer = &Warmer[lcv];

		/* handle the dimming of the LED green during the bag out countdown */
		if (Warmer[lcv].BAG_IN == false && Warmer[lcv].STATE == eREADY) {

//			This loop compensates for nonlinear dimming
//			start by dropping by -5
			if (Warmer[lcv].READY_BRIGHTNESS > 100) {

				Warmer[lcv].READY_BRIGHTNESS = (Warmer[lcv].READY_BRIGHTNESS) - 5;
			}

//			if brightness is larger than 2, decrement brightness by 2
			else if (Warmer[lcv].READY_BRIGHTNESS >= 2) {

				Warmer[lcv].READY_BRIGHTNESS = (Warmer[lcv].READY_BRIGHTNESS) - 2;
			}

//			if the brightness is less than 2, jump to 0 to eliminate the possibility of rollover
			else {

			Warmer[lcv].READY_BRIGHTNESS = 0;
			}
		}

		switch (ptrWarmer->STATE) {
		case eWARMING:
			SetLEDColor(lcv, 0x00, 0x00, blue); /* blue */
			break;

		case eREADY:
			SetLEDColor(lcv, 0x00, Warmer[lcv].READY_BRIGHTNESS, 0x00); /* green */
			break;

		case eREADY_COUNTDOWN:
			SetLEDColor(lcv, 0x00, Warmer[lcv].READY_BRIGHTNESS, 0x00); /* slowly dimming green */
			break;

		case eEXPIRED:
			SetLEDColor(lcv, 0xFF, 0x40, 0x00); /* warning yellow */
			break;

		case eINOP:
			SetLEDColor(lcv, red_brightness, green_brightness, 0x00); /* warning yellow flashing */
			break;

		case eEMPTY:
		default:
			SetLEDColor(lcv, 0x00, 0x00, 0x00); /* all off */
			break;
		}
	}

	/*push updates to LED array */
	WS2812_OutputDataDMA(led_data, sizeof(led_data) / sizeof(int32_t));
}
