/*
 * prox.c
 *
 *  Created on: Jan 23, 2020
 *      Author: JEFPL8
 */

#include "Includes.h"

///* Define parameters of 1L bag in overwrap, assumed to be worst (slowest) case */
#define Bag_volume 0.001f      //  1L in cubic meters
#define Bag_area 0.0689f       //  surface area in square meters
#define Bag_storing_temp 25.0f //  25C based on various manufaturers info, see 735162-070 for complied datasheets
#define Elasped_time 10.0f     //  this calculation is called every 10 sec, so how much energy has been transfered in the last 10 sec
#define Conv_coef  15.0f       //  empirically derived convection coefficient of a 1L bag in overwrap
#define Spec_heat_H2O 4185.5f  //  specific heat of water (J/kg deg K), because 1L weighs 1kg, units can be J/ degree K

//loop takes 0.214ms
void Calc_heat(void) {
//	PTE->PSOR = 1UL << 9;//indicate timer increment
	for (uint8_t lcv = 0; lcv < eWARMER_COUNT; lcv++) {

		// if there is a bag in warming state...
		if (Warmer[lcv].STATE == eWARMING) {

			// ...calculate the energy (J) transfered to this bag over the last time interval
			float energy_transfer = (Conv_coef
					* (Bag_area * (Temperature.cavity_temp - Warmer[lcv].BAG_TEMP))) * Elasped_time;

			// and then calculate the new bag temperature with the added thermal energy
			Warmer[lcv].BAG_TEMP = Warmer[lcv].BAG_TEMP
					+ (energy_transfer / Spec_heat_H2O);

		}
	}
//	PTE->PCOR = 1UL << 9;//indicate timer increment
}
