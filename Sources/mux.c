/*
 * mux.c
 *
 *  Created on: Oct 2, 2019
 *      Author: JEFPL8
 */

#include "Includes.h"

#define TRANSFER_SIZE   1u
///* Define data transfer size */
//lpi2c_master_state_t lpi2c1MasterState;

uint8_t SET_TO_PORT_ZERO[] = { 0 };

void Init_mux(void) {
	/* drive mux reset line low to perform a power on reset */
	PINS_DRV_WritePin(PTE, 10, 0);

	/* drive mux reset line high   */
	PINS_DRV_WritePin(PTE, 10, 1);

	/* Initialize both muxs to have no port selected */
	LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, 0x70, false);
	LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, SET_TO_PORT_ZERO,
			TRANSFER_SIZE,
			true, 10u);

	LPI2C_DRV_MasterSetSlaveAddr(INST_LPI2C, 0x77, false);
	LPI2C_DRV_MasterSendDataBlocking(INST_LPI2C, SET_TO_PORT_ZERO,
			TRANSFER_SIZE,
			true, 10u);

}
